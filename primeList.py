#Bahar Gürsoy

def primeList(upperBound):
    primeList = []
    for i in range(2,upperBound):
        primeList.append(i)
        for n in range(2,upperBound):
            if n < i and i%n == 0:
                primeList.remove(i)
                break
    return primeList